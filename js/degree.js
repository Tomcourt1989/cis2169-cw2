function loadJSON(callback) {   

    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', 'js/module-1.json', true);
    xobj.onreadystatechange = function () {
        if (xobj.readyState == 4 && xobj.status == "200") {
            callback(xobj.responseText);
        }
    };
    xobj.send(null);  
}

function init() {
    loadJSON(function(response) {
        var JSONdata = JSON.parse(response);
        var text = "";

        for(i = 0; i < JSONdata.length; i++) {
            text += "<p>" + JSONdata[i].Course + "</p>"
        }
        
        if(localStorage.name != null) {
            text += "<p>" + localStorage.getItem('name') + "</p>";
        }
        document.getElementById('New_Degree').innerHTML = text;


    });
}

var DLO = 1;
function add_fields() {
  DLO++;
  var objTo = document.getElementById('DLO_fields')
  var box = document.createElement("div");
  box.innerHTML = '<div class="label">Learning Outcome ' + DLO +':</div><div class="content"><input type="text" id="DLO" value="" />';
    
  objTo.appendChild(box)
}
        

function save_form() {
    var DLO = document.getElementById('DLO');
    var degree = document.getElementById('degree');
    var module = document.getElementById('module');
    var exits = document.getElementById('exits');

    localStorage.setItem('DLO', DLO.value);
    localStorage.setItem('degree', degree.value);
    localStorage.setItem('module', module.value);
    localStorage.setItem('exits', exits.value);
}

function post_formd() {
    var getDegree = localStorage.getItem('degree');
    var getModule = localStorage.getItem('module');
    var getExits = localStorage.getItem('exits');
    var getDLO = localStorage.getItem('DLO');

    if(localStorage.degree != null)
        document.getElementById('New_Degree').innerHTML = "<p>Degree Name:<br>" + getDegree + 
        "</p><p>Module name:<br>" + getModule + "</p><p>Exit Awards:<br>" + getExits + "</p><p>Learning Outcomes:<br>" + getDLO + "</p>";
}

