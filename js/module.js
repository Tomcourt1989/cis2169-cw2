var nameObj = {
    "Server and Client Side Scripting": {
      "Dave Walsh": [5, 10, 15, 20],
      "James Smith": [10, 20],
      "Sarah White": [20]    
    },
    "Web Application Development": {
      "Hari Pandey": [10, 20],
      "Emma Brown": [5, 10, 15, 20]
    }
  }
  window.onload = function() {
    var nameSel = document.getElementById("name");
    var academicSel = document.getElementById("academic");
    var creditSel = document.getElementById("credits");
    for (var x in nameObj) {
      nameSel.options[nameSel.options.length] = new Option(x, x);
    }
    nameSel.onchange = function() {

      creditSel.length = 1;
      academicSel.length = 1;
      for (var y in nameObj[this.value]) {
        academicSel.options[academicSel.options.length] = new Option(y, y);
      }
    }
    academicSel.onchange = function() {

      creditSel.length = 1;

      var z = nameObj[nameSel.value][this.value];
      for (var i = 0; i < z.length; i++) {
        creditSel.options[creditSel.options.length] = new Option(z[i], z[i]);
      }
    }
  }

  var LO = 1;
  function add_fields() {
    LO++;
    var objTo = document.getElementById('LO_fields')
    var box = document.createElement("div");
    box.innerHTML = '<div class="label">Learning Outcome ' + LO +':</div><div class="content"><input type="text" id="LO" value="" />';
      
    objTo.appendChild(box)
  }
          
  
function save_form() {
  var name = document.getElementById('name');
  var academic = document.getElementById('academic');
  var hours = document.getElementById('hours');
  var credits = document.getElementById('credits');
  var LO = document.getElementById('LO');
  var degree = document.getElementById('degree');
  var dmodule = document.getElementById('module');
  var exits = document.getElementById('exits');

  localStorage.setItem('name', name.value);
  localStorage.setItem('academic', academic.value);
  localStorage.setItem('hours', hours.value);
  localStorage.setItem('credits', credits.value);
  localStorage.setItem('LO', LO.value);
  localStorage.setItem('degree', degree.value);
  localStorage.setItem('dmodule', dmodule.value);
  localStorage.setItem('exits', exits.value);
}

function post_form() {
  var getName = localStorage.getItem('name');
  var getAcademic = localStorage.getItem('academic');
  var getHours = localStorage.getItem('hours');
  var getCredits = localStorage.getItem('credits');
  var getLO = localStorage.getItem('LO');

  if(localStorage.name != null)
    document.getElementById('New_Module').innerHTML = "<p>Module Name:<br>" + getName + "</p><p>Academic:<br>" + getAcademic + "</p><p>Hours:<br>" + getHours + "</p><p>Credits:<br>" + getCredits + "</p><p>Learning Outcomes:<br>" + getLO + "</p>";
}